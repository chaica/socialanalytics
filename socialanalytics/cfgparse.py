# -*- coding: utf-8 -*-
# Copyright © 2015-2017 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get values of the configuration file
'''Get values of the configuration file'''

# standard libraries imports
import configparser
import os.path
import sys

# app libraries imports
from socialanalytics.cfgparsers.mastodon import parsemastodon
from socialanalytics.cfgparsers.graph import parsegraph

def parseconfig(pathtoconf):
    '''Parse the configuration'''
    config = configparser.ConfigParser()
    try:
        with open(pathtoconf) as conffile:
            config.read_file(conffile)
            # parse twitter configuration
            graphconf = parsegraph(config)
            mastodonconf = parsemastodon(config)
    except (configparser.Error, IOError, OSError) as err:
        sys.exit(err)
    # parsed configuration values to return
    return {'graph': graphconf,
            'mastodon': mastodonconf}
