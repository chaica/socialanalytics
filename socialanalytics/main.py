#!/usr/bin/env python3
# vim:ts=4:sw=4:ft=python:fileencoding=utf-8
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

'''Get data from social networks and feed a graph'''

# standard libraires imports
import logging
import logging.handlers
import sys

# app libraries imports
from socialanalytics.cliparse import parsecli
from socialanalytics.cfgparse import parseconfig
from socialanalytics.followers import get_followers
from socialanalytics.unfollowers import get_unfollowers
from socialanalytics.graph import AccountGraph

class Main:
    '''Main class of SocialAnalytics'''

    def __init__(self):
        self.main()

    def setup_logging(self, cliargs):
        '''setup the logging parameters'''
        if cliargs.syslog:
            sl = logging.handlers.SysLogHandler(address='/dev/log')
            sl.setFormatter(logging.Formatter('socialanalytics[%(process)d]: %(message)s'))
            # convert syslog argument to a numeric value
            loglevel = getattr(logging, cliargs.syslog.upper(), None)
            if not isinstance(loglevel, int):
                raise ValueError('Invalid log level: %s' % loglevel)
            sl.setLevel(loglevel)
            logging.getLogger('').addHandler(sl)
            logging.debug('configured syslog level %s' % loglevel)
        logging.getLogger('').setLevel(logging.DEBUG)
        sh = logging.StreamHandler()
        sh.setLevel(cliargs.log_level.upper())
        logging.getLogger('').addHandler(sh)
        logging.debug('configured stdout level %s' % sh.level)

    def main(self):
        '''The main function'''
        cliargs = parsecli()
        self.setup_logging(cliargs)
        for config in cliargs.configs:
            cfg = parseconfig(config)
            followers = get_followers(cfg['mastodon'])
            g = AccountGraph(cfg)
            g.store_followers(followers)
            followers_from_graph = g.get_followers()
            unfollowers = get_unfollowers(followers, followers_from_graph) 
            g.store_unfollowers(unfollowers)
        sys.exit(0)
