# -*- coding: utf-8 -*-
# vim:ts=4:sw=4:ft=python:fileencoding=utf-8
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get followers from Mastodon
'''Get followers from Mastodon'''

# standard libraries imports
import os.path
import sys

# 3rd party libraries imports
from mastodon import Mastodon
from py2neo import Node, Relationship

def get_unfollowers(followers_from_mastodon, followers_from_graph):
    '''Compare followers from Mastodon and local followers to get the unfollowers'''
    mastofollowers = set(followers_from_mastodon)
    graphfollowers = set(followers_from_graph)
    unfollowers = mastofollowers ^ graphfollowers
    return unfollowers
