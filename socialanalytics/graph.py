# -*- coding: utf-8 -*-
# vim:ts=4:sw=4:ft=python:fileencoding=utf-8
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Graph of followers
'''Graph of the followers'''

# standard libraries imports
from datetime import datetime
import logging
import os.path
import sys

# 3rd party libraries imports
from py2neo import Graph, Node, Relationship, NodeMatcher, RelationshipMatcher
#from py2neo.selection import NodeSelector

class AccountGraph:
    def __init__(self, config):
        account = config['mastodon']['account']
        # initiate the object graph
        self.graph = Graph(host=config['graph']['host'], user=config['graph']['user'], password=config['graph']['pass'], keep_alive=True)
        self.account = account
        # create the nodeselector
        self.matcher = NodeMatcher(self.graph)
        self.rmatcher = RelationshipMatcher(self.graph)
        # generate date
        self.datenow = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        # if the end node does not exist, create it
        account_exists, accountnode_from_graph = self.account_exists(self.account)
        if not account_exists:
            tx = self.graph.begin()
            self.accountnode = Node('Account', account=self.account)
            tx.create(self.accountnode)
            tx.commit()
        else:
            self.accountnode = accountnode_from_graph

    def store_followers(self, followers):
        '''Store followers'''
        followernb = 0
        tx = self.graph.begin()
        for follower in followers:
            # tests
            account_exists, followernode_from_graph = self.account_exists(follower)
            if account_exists:
                #followernodefromgraph = self.get_node_from_graph(follower)
                is_followed_by = self.is_followed_by(follower)
            else:
                is_followed_by = False
            # if the account does not exist, create it and link it to me
            if not account_exists:
                followernode = Node('Account', account=follower)
                tx.create(followernode)
                rel = Relationship(followernode, 'FOLLOWS', self.accountnode, since=[self.datenow])
                tx.create(rel)
                followernb += 1
            # if the account exists but not followed, create the relationship
            if account_exists and not is_followed_by:
                rel = Relationship(followernode_from_graph, 'FOLLOWS', self.accountnode, since=[self.datenow])
                tx.create(rel)
                followernb += 1
            if account_exists and is_followed_by:
                is_unfollowed_by = self.is_unfollowed_by(follower)
                if is_unfollowed_by:
                    # the follower was following, then unfollowing and now following again
                    # if unfollow timestamp is more recent than latest follow timestamp, store follow timestamp 
                    follow_relationship = self.get_follow_relationship_from_graph(follower)
                    unfollow_relationship = self.get_unfollow_relationship_from_graph(follower)
                    latest_follow_string = follow_relationship['since'][-1]
                    latest_follow_timestamp = datetime.strptime(latest_follow_string, '%Y-%m-%d_%H-%M-%S')
                    latest_unfollow_string = unfollow_relationship['since'][-1]
                    latest_unfollow_timestamp = datetime.strptime(latest_unfollow_string, '%Y-%m-%d_%H-%M-%S')
                    # comparer latest follow timestamp and latest unfollow timestamp
                    # add current date if unfollow was the latest action (contrary means we already stored this follow timestamp
                    if latest_follow_timestamp < latest_unfollow_timestamp:
                        follow_relationship['since'].append(self.datenow)
                        tx.push(follow_relationship)
        # commit the transaction and return
        tx.commit()
        return followernb

    def store_unfollowers(self, unfollowers):
        '''Store unfollowers'''
        unfollowernb = 0
        tx = self.graph.begin()
        for unfollower in unfollowers:
            account_exists, unfollowernode_from_graph = self.account_exists(unfollower)
            if account_exists:
                #unfollowernodefromgraph = self.get_node_from_graph(unfollower)
                is_followed_by = self.is_followed_by(unfollower)
                is_unfollowed_by = self.is_unfollowed_by(unfollower)
            else:
                is_followed_by = False
                is_unfollowed_by = False
            # if the account exists but not unfollowed, create the relationship
            if account_exists and not is_unfollowed_by:
                if account_exists and is_followed_by:
                    #unfollowernode = self.get_node_from_graph(unfollower)
                    rel = Relationship(unfollowernode_from_graph, 'UNFOLLOWS', self.accountnode, since=[self.datenow])
                    tx.create(rel)
                    unfollowernb += 1
            if account_exists and is_unfollowed_by:
                is_followed_by = self.is_followed_by(unfollower)
                if is_followed_by:
                    #relationship = self.get_unfollow_relationship_from_graph(unfollower)
                    #relationship['since'].append(self.datenow)
                    #tx.push(relationship)
                    # the unfollower was unfollowing, then following and now unfollowing again
                    # if follow timestamp is more recent than latest unfollow timestamp, store unfollow timestamp 
                    follow_relationship = self.get_follow_relationship_from_graph(unfollower)
                    unfollow_relationship = self.get_unfollow_relationship_from_graph(unfollower)
                    latest_follow_string = follow_relationship['since'][-1]
                    latest_follow_timestamp = datetime.strptime(latest_follow_string, '%Y-%m-%d_%H-%M-%S')
                    latest_unfollow_string = unfollow_relationship['since'][-1]
                    latest_unfollow_timestamp = datetime.strptime(latest_unfollow_string, '%Y-%m-%d_%H-%M-%S')
                    # comparer latest follow timestamp and latest unfollow timestamp
                    # add current date if unfollow was the latest action (contrary means we already stored this follow timestamp
                    if latest_follow_timestamp > latest_unfollow_timestamp:
                        unfollow_relationship['since'].append(self.datenow)
                        tx.push(unfollow_relationship)
        # commit the transaction and return
        tx.commit()
        return unfollowernb

    def get_followers(self):
        '''get all followers'''
        query = 'MATCH (n) -[:FOLLOWS]-> (:Account {{account: "{account}"}}) return (n.account)'.format(account=self.account)
        res = []
        for i in self.graph.run(query):
            res.append(i.data()['(n.account)'])
        return res

    def account_exists(self, account):
        '''check if an account exists'''
        selected = self.matcher.match('Account', account=account).first()
        if not selected:
            return False, None
        else:
            return True, selected

    def is_followed_by(self, follower):
        '''check if the account follows the main user'''
        query = 'MATCH (n :Account {{account: "{follower}"}}) -[:FOLLOWS]-> (:Account {{account: "{account}"}}) return (n.account)'.format(follower=follower, account=self.account)
        found = self.graph.run(query).evaluate()
        if not found:
            return False
        else:
            return True

    def is_unfollowed_by(self, unfollower):
        '''check if the account unfollowed the main user'''
        query = 'MATCH (n :Account {{account: "{unfollower}"}}) -[:UNFOLLOWS]-> (:Account {{account: "{account}"}}) return (n.account)'.format(unfollower=unfollower, account=self.account)
        found = self.graph.run(query).evaluate()
        if not found:
            return False
        else:
            return True

    def get_node_from_graph(self, follower):
        '''get node from database'''
        return self.matcher.match('Account', account=follower).first()

    def get_follow_relationship_from_graph(self, follower):
        '''get relationship from graph'''
        query = 'MATCH (:Account {{account: "{follower}"}}) -[r :FOLLOWS]-> (:Account {{account: "{account}"}}) return (r)'.format(follower=follower, account=self.account)
        relationship = self.graph.run(query).evaluate()
        return relationship

    def get_unfollow_relationship_from_graph(self, unfollower):
        '''get unfollow relationship from graph'''
        query = 'MATCH (:Account {{account: "{unfollower}"}}) -[r :UNFOLLOWS]-> (:Account {{account: "{account}"}}) return (r)'.format(unfollower=unfollower, account=self.account)
        relationship = self.graph.run(query).evaluate()
        return relationship
