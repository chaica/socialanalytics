# -*- coding: utf-8 -*-
# vim:ts=4:sw=4:ft=python:fileencoding=utf-8
# Copyright © 2018 Carl Chenet <carl.chenet@ohmytux.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/

# Get followers from Mastodon
'''Get followers from Mastodon'''

# standard libraries imports
import os.path
import sys

# 3rd party libraries imports
from mastodon import Mastodon
from py2neo import Node, Relationship

def get_followers(mastoconf):
    '''Use Mastodon credentials to get the the number of followers'''
    followers = []
    instance = mastoconf['instance']
    mastodon = Mastodon(
        client_id=mastoconf['client_credentials'],
        access_token=mastoconf['user_credentials'],
        api_base_url='https://{instance}'.format(instance=instance)
    )
    for prefix in ('http:', 'https:'):
        if instance.startswith(prefix):
            instance = instance.replace(prefix, '')
    userid = mastodon.account_verify_credentials()['id']
    res = mastodon.account_followers(userid)
    for page in mastodon.fetch_remaining(res):
        username = page['acct']
        if '@' not in username:
            username = '@'.join([username, instance])
        followers.append(username)
    return followers
