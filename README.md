### SocialAnalytics

SocialAnalytics gets followers and relationships and feeds a Neo4j database.
For the full documentation, [read it online](https://socialanalytics.readthedocs.org/en/latest/).

If you would like, you can [support the development of this project on Liberapay](https://liberapay.com/carlchenet/).
Alternatively you can donate cryptocurrencies:

- BTC: 1AW12Zw93rx4NzWn5evcG7RNNEM2RSLmAC
- XMR: 43GGv8KzVhxehv832FWPTF7FSVuWjuBarFd17QP163uxMaFyoqwmDf1aiRtS5jWgCiRsi73yqedNJJ6V1La2joznKHGAhDi

### Quick Install

* Install SocialAnalytics from PyPI

        # pip3 install socialanalytics

* Install SocialAnalytics from sources
  *(see the installation guide for full details)
  [Installation Guide](http://socialanalytics.readthedocs.org/en/latest/install.html)*


        # tar zxvf socialanalytics-0.7.tar.gz
        # cd socialanalytics
        # python3 setup.py install
        # # or
        # python3 setup.py install --install-scripts=/usr/bin

### Create the authorization for the Feed2toot app

* Just launch the following command::

        $ register_socialanalytics_app

### Use SocialAnalytics

* Create or modify socialanalytics.ini file in order to configure socialanalytics:

        [mastodon]
        account=johndoo@instance.social
        user_credentials=/home/johndoo/socialanalytics/johndoo_usercred.txt
        client_credentials=/home/johndoo/socialanalytics/s/johndoo_clientcred.txt

        [graph]
        user=socialanalytics
        pass=OoWei6chii2raini

* Launch SocialAnalytics

        $ socialanalytics -c /path/to/socialanalytics.ini

### Authors

* Carl Chenet <chaica@ohmytux.com>

### License

This software comes under the terms of the GPLv3+.
